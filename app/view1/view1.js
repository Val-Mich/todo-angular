'use strict';

angular.module('myApp.view1', ['ui.router', 'ngMaterial', 'ngResource'])

    .config(['$stateProvider', '$resourceProvider', function ($stateProvider, $resourceProvider) {
        $resourceProvider.defaults.stripTrailingSlashes = false;
        $stateProvider.state('view1', {
            url: "/view1",
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl',
            resolve: {
                todoList: function ($http, $resource, $timeout) {
                    //json-server
                    return $resource('http://localhost:3000/todo').query().$promise;
                    // return $http.get("data/data.json").then(function (data) {
                    //     return $timeout(function () {
                    //         return data;
                    //     },1500)
                    // });
                }
            }
        });
    }])

    .service('todoResource', function ($resource) {
        return $resource('http://localhost:3000/todo/:id', {id:'@id'}, {
            'get': {method: 'GET'},
            'save': {method: 'POST'},
            'query': {method: 'GET', isArray: true},
            'remove': {method: 'DELETE'},
            'update': {method: 'PATCH'},
            'delete': {method: 'DELETE'}
        });
    })

    .factory('focus', function ($timeout, $window) {
        return function (id) {
            // timeout makes sure that is invoked after any other event has been triggered.
            // e.g. click events that need to run before the focus or
            // inputs elements that are in a disabled state but are enabled when those events
            // are triggered.
            $timeout(function () {
                var element = $window.document.getElementById(id);
                if (element)
                    element.focus();
            });
        };
    }).filter('startFrom', function () {
        return function (input, start) {
            start = +start; //parse to int
            return input.slice(start);
        };
    })
    .directive('myListItem', function (focus) {

        function link(scope, element, attrs) {
            scope.delete = function ($todo) {
                scope.deleteCallBack($todo);
            };
            scope.edit = function ($todo) {
                scope.editCallBack($todo);
            };
            scope.complete = function ($todo) {
                scope.completeCallBack($todo);
            };
        }

        return {
            scope: {
                data: "=list",
                deleteCallBack: "=deleteCallBack",
                editCallBack: "=editCallBack",
                completeCallBack: "=completeCallBack",
                selectedIndex: "=selectedIndex",
                index: "=index",
                item: "=item",
            },
            restrict: "EA",
            templateUrl: "/template/myListItem.html",
            link: link
        }
    })
    .controller('View1Ctrl', ['$scope', '$http', '$filter', 'todoList', 'todoResource', function ($scope, $http, $filter, todoList, todoResource) {

        $scope.data = todoList.sort(function (a, b) {
            return;
        });
        ;
        $scope.pageSize = 5;
        $scope.currentPage = 0;
        $scope.searchText = "";
        $scope.todoLabel = "Nouveau Todo";
        $scope.action = "Ajouter";
        $scope.newEntry = "";
        $scope.modify = false;
        $scope.modifyId = null;
        $scope.inputId = "inputTodo";
        $scope.selectedIndex = null;

        $scope.getData = function () {
            return $filter('filter')($scope.data, {title: $scope.searchText});
        };

        $scope.numberOfPages = function () {
            return Math.ceil($scope.getData().length / $scope.pageSize);
        };

        $scope.getNumber = function (num) {
            return new Array(num);
        };

        $scope.deleteCallBack = function ($todo) {
            todoResource.delete($todo, function () {
                $scope.data.splice($scope.data.indexOf($todo), 1);
                if ($scope.numberOfPages() < ($scope.currentPage + 1)) {
                    $scope.currentPage--;
                }
            }, function () {
                console.log("fail");
            });
        };

        $scope.completeCallBack = function ($todo) {
            var todoR = todoResource.get($todo, function () {
                todoR.completed = !$todo.completed;
                todoResource.update(todoR, function () {
                    $todo.completed = !$todo.completed;
                    $scope.reInitInput();
                }, function () {
                    console.log('fail');
                });
            }, function () {
                console.log('fail');
            });
        };

        $scope.editCallBack = function ($todo) {
            $scope.todoLabel = "Editer Todo";
            $scope.action = "Modifier";
            $scope.newEntry = $todo.title;
            $scope.modify = true;
            document.getElementById($scope.inputId).focus();
            $scope.modifyId = $todo.id;
            $scope.selectedIndex = $todo;
        };

        $scope.goPageOne = function () {
            $scope.goToPage(0);
        };

        $scope.goToPage = function ($index) {
            $scope.currentPage = $index
        };

        $scope.goLastPage = function () {
            var nbPage = $scope.numberOfPages();
            if (nbPage < $scope.currentPage + 1) {
                $scope.goToPage(nbPage - 1);
            }
        };

        $scope.add = function () {
            if ($scope.modify) {
                var todoR = todoResource.get({id: $scope.modifyId}, function () {
                    todoR.title = $scope.newEntry;
                    todoResource.update(todoR, function () {
                        $scope.selectedIndex.title = $scope.newEntry;
                        $scope.reInitInput();
                    }, function () {
                        console.log('fail');
                    });
                }, function () {
                    console.log('fail');
                });
            } else {
                todoResource.save({title: $scope.newEntry, completed: false}, function () {
                    $scope.data.unshift({title: $scope.newEntry});
                }, function () {
                    console.log('fail');
                });
            }
        };

        $scope.reInitInput = function () {
            $scope.selectedIndex = null;
            $scope.modifyId = null;
            $scope.modify = false;
            $scope.todoLabel = "Nouveau Todo";
            $scope.action = "Ajouter";
            $scope.newEntry = "";
        }

    }]);